﻿using Lesson1.Models;
using Lesson1.Service;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Lesson1.Controllers
{
    public class PeopleController : Controller
    {

        PeopleService _ps = new PeopleService();

        //общий список людей
        public ActionResult PeopleList()
        {
            //экземпляр
            Person person = new Person();

            //проводники данных
            ViewBag.Name = _ps.GetAttribute("Name", person);
            ViewBag.Position = _ps.GetAttribute("Position", person);
            ViewBag.People = _ps.GetPeople();
            return View();
        }


        //детали о каждом
        public ActionResult PersonDetails(int id)
        {
            //ViewBag.PersonDetails = _ps.GetPerson(id);
            return View(_ps.GetPerson(id));
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult Create()
        {

            return View();

        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Create(Person person)
        {
            if (!ModelState.IsValid)
            {
                return View(person);
            }

            _ps.AddPerson(person);
            return RedirectToAction("PeopleList");

        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult Edit(int id)
        {

            return View(_ps.GetPerson(id));

        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Edit(Person person)
        {
            if (!ModelState.IsValid)
            {
                return View(person);
            }

            _ps.EditPerson(person);
            return RedirectToAction("PeopleList");

        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(Person person)
        {
            _ps.DeletePerson(person);
            return RedirectToAction("PeopleList");

        }
    }



}