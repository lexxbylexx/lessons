﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lesson1.Models
{
    public class Person
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле для обязательного заполнения")]
        [MinLength(2, ErrorMessage = "Минимальная длина не более 2 символа")]
        [MaxLength(32, ErrorMessage = "Максимальная длина не более 32 символа")]
        [Description("Ф.И.О.")]
        [Display(Name = "Ф.И.О.")]

        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле для обязательного заполнения")]
        [Range(18,65,ErrorMessage = "Возраст введен неверно")]
        [Description("Возраст")]

        [Display(Name = "Возраст")]
        public int Age { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле для обязательного заполнения")]
        [MinLength(2, ErrorMessage = "Минимальная длина не более 2 символа")]
        [MaxLength(32, ErrorMessage = "Максимальная длина не более 32 символа")]
        [Description("Должность")]

        [Display(Name = "Должность")]
        public string Position{ get; set; } 
        [DataType(DataType.EmailAddress,ErrorMessage = "Неверный формат эмэйла")]
        public string Email { get; set; }
      

    }
}