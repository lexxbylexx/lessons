﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lesson1.Models.User
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}