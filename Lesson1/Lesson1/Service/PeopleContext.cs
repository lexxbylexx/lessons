﻿using Lesson1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Lesson1.Service
{
    public class PeopleContext : DbContext
    {
        public PeopleContext()
           : base("DbConnection")
        { }

        public DbSet<Person> Peoples { get; set; }
    }
}