﻿using Lesson1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace Lesson1.Service
{
    public class PeopleService
    {
        AttributeCollection attributes;
        PeopleContext _db = new PeopleContext();


        //добавить сотрудника
        public void AddPerson(Person person)
        {

            _db.Peoples.Add(person);
            _db.SaveChanges();
        }

        //Инициализатор модели
        public Person Init(Person person)
        {
            return
                new Person {
                    Name = person.Name,
                    Age = person.Age,
                    Email = person.Email,
                    Position = person.Position };
        }

        //добавить сотрудника
        public void EditPerson(Person person)
        {
            var result = _db.Peoples.SingleOrDefault(b => b.Id == person.Id);
            if (result != null)
            {
                result.Name = person.Name;
                result.Email = person.Email;
                result.Age = person.Age;
                result.Position = person.Position;
                _db.SaveChanges();
            }
            else
            {
                //не найдено в БД
            }

        }

        public void DeletePerson(Person person)
        {
            var result = _db.Peoples.SingleOrDefault(b => b.Id == person.Id);
            if (result != null)
            {
                _db.Peoples.Remove(result);
                _db.SaveChanges();
            }
            else
            {
                //не найдено в БД
            }

        }

        //получить значение атрибута
        public string GetAttribute(string Prop, Person Path)
        {
 
            attributes = TypeDescriptor.GetProperties(Path)[Prop].Attributes;

            DescriptionAttribute thisAttribute =
            (DescriptionAttribute)attributes[typeof(DescriptionAttribute)];

            return thisAttribute.Description;
        }

        //получить персональные данные сотрудника
        public Person GetPerson(int personId)
        {
           //нужна проверка на null
            return _db.Peoples.FirstOrDefault(x => x.Id == personId);
        }

        //получить коллекцию сотрудников
        public IEnumerable<Person> GetPeople()
        {
            return _db.Peoples.OrderBy(x => x.Name).ToList();
        }


    }
}